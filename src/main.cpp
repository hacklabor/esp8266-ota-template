#include <Arduino.h>

#if defined(ESP8266)
  #include <ESP8266WiFi.h>
  #include <WiFiClient.h>
  #include <ESP8266WebServer.h>
#elif defined(ESP32)
  #include <WiFi.h>
  #include <WiFiClient.h>
  #include <WebServer.h>
#endif

#include <ElegantOTA.h>
#include <secret-Template.h>


#if defined(ESP8266)
  ESP8266WebServer server(80);
#elif defined(ESP32)
  WebServer server(80);
#endif

void Wifi_ElegantOTA()
{
  WiFi.mode(WIFI_STA);
  WiFi.setHostname(STAHOST);
  WiFi.begin(STASSID, STAPSK);
  WiFi.setHostname(STAHOST);

  while (WiFi.waitForConnectResult() != WL_CONNECTED)
  {
    // Serial.println("Connection Failed! Rebooting...");
    delay(5000);
  }

    server.on("/", []() {
    server.send(200, "text/plain", "Hi! I am ESP8266 \n OTA Update unter http://[hostname]/update ");
  });


  ElegantOTA.begin(&server); // Start ElegantOTA
  server.begin();
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());
}

void setup() {
  Serial.begin(115200);
  Wifi_ElegantOTA();
 
}

void loop() {
  server.handleClient();
}